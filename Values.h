//
// Created by monteth on 10/24/17.
//

#ifndef ZMPO_2_VALUES_H
#define ZMPO_2_VALUES_H

const std::string  sEND = "end";
const std::string  sHELP = "help";
const std::string  sADDMAT = "addmat";
const std::string  sLIST = "list";
const std::string  sDELLALL = "delall";
const std::string  sPRINT = "print";
const std::string  sDEF = "def";
const std::string  sCLONE = "clone";
const std::string  sRENAME = "rename";
const std::string  sDEL = "del";
const std::string  WRNG_COMM = "Wrong command or invalid parameter, to see commands definitions write \"help\"";
const std::string  sCONSTR_CELL = "Constructed object, value: ";

const std::string  sCONSTR_MATRIX = "Constructed matrix named ";
const std::string  sDESTR_CELL = "Destructed object, value: ";


const std::string  sDESTR_MATRIX = "Constructed matrix named ";
const std::string  sDESTR = "destroyed: ";
const std::string  sCOPIED_MATRIX = "_copy";

const std::string DEFAULT_NAME = "def_name";

const int COMM_TYPE_4 = 4;
const int COMM_TYPE_3 = 3;
const int COMM_TYPE_2 = 2;
const int COMM_MATRIX_OFFSET = 1;
const int COMM_CELL_OFFSET = 2;
const int COMM_VALUE_OFFSET = 3;
const int INT_LENGTH = 8;

const std::string  HELP_TO_PRINT =
        "List of possible commands and it's parameters:\naddmat <dimNum> <dim0size> <dim1size>… <dimNum-1size> <def> <!name!> - add an matrix with <dimNum> dimensials, in range <dimXsize> and default value = <def>\nlist - show matrixes\ndel <offset> - delete matrix with on <offset>\ndelall - delete all matrixes\nhelp - show list of commands\ndef <matoff> <dim0> <dim1>… <dimNum-1> <val> - set value <val> for matrix on offset <matoff> and coordinates <dimX>\nprint <matoff> - print matrix on offset <offset>\nclone <offset> - clone matrix from offset <offset> and add to list\nrename <offset> <newname> - rename matrix on offset <offset> to name <newname>\nexit - shout down program\n";


#endif //ZMPO_2_VALUES_H
