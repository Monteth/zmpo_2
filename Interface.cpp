//
// Created by monteth on 10/24/17.

#include "Matrix/MatrixManager.h"
#include "Values.h"
#include "Interface.h"

using namespace std;


void Interface::start() {
    std::string line;

    MatrixManager manager;
    bool end = false;
    int commandsLength = 0;
    std::string *commandsArray;
    string command;
    printHelp();
    do {
        commandsLength = 0;
        getline(std::cin, line);
        commandsArray = split(line, ' ', &commandsLength);
        command = commandsArray[0];
        bool success = false;

        if (line == sEND) {
            end = true;
            success = true;
            manager.delall();

        } else if (line == sHELP) {
            printHelp();
            success = true;

        } else if (command == sADDMAT) {
            if (commandsLength > 2) {
                if (isInt(commandsArray[1])) {
                    int quantOfDim = stoi(commandsArray[1]);
                    if (commandsLength ==
                        quantOfDim + COMM_TYPE_4) { // quantOfDim + dims(quantOfDim) + command + defVal + name
                        if (tabIsInt(commandsArray, 2, quantOfDim)) {
                            int *dimens = strToIntArray(commandsArray, 2, quantOfDim);
                            manager.addMatrix(quantOfDim, dimens, stoi(commandsArray[commandsLength - 2]),
                                              commandsArray[commandsLength - 1]);
                            success = true;
                        }
                    } else if (commandsLength ==
                               quantOfDim + COMM_TYPE_3) { // quantOfDim + dims(quantOfDim) + command + defVal
                        if (tabIsInt(commandsArray, 2, quantOfDim)) {
                            int *dimens = strToIntArray(commandsArray, 2, quantOfDim);
                            manager.addMatrix(quantOfDim, dimens, stoi(commandsArray[commandsLength - 1]));
                            success = true;
                        }
                    }
                }
            }
        } else if (line == sLIST) {
            manager.printList();
            success = true;
        } else if (line == sDELLALL) {
            manager.delall();
            success = true;
        } else if (command == sDEF) {
            if (commandsLength > 2) {
                if (isInt(commandsArray[COMM_MATRIX_OFFSET])) {
                    int matrixOffset = stoi(commandsArray[COMM_MATRIX_OFFSET]);
                    if (matrixOffset < manager.getQuantOfMatrixes()) {
                        //long def
                        if (commandsLength == manager.getQuantOfDim(matrixOffset) + COMM_TYPE_3 &&
                            isInt(commandsArray[commandsLength - 1])) {
                            if (tabIsInt(commandsArray, 2, manager.getQuantOfDim(matrixOffset))) {
                                int *dimens = strToIntArray(commandsArray, 2, manager.getQuantOfDim(matrixOffset));
                                manager.def(matrixOffset, dimens, stoi(commandsArray[commandsLength - 1]));
                                success = true;
                            }
                        }
                    }
                }
            }
        } else if (command == sPRINT) {
            if (commandsLength == COMM_TYPE_2) {
                if (isInt(commandsArray[1])) {
                    int offset = stoi(commandsArray[1]);
                    if (offset < manager.getQuantOfMatrixes()) {
                        manager.print(offset);
                        success = true;
                    }
                }
            }
        } else if (command == sCLONE) {
            if (commandsLength == COMM_TYPE_2) {
                if (isInt(commandsArray[1])) {
                    int offset = stoi(commandsArray[1]);
                    if (offset < manager.getQuantOfMatrixes()) {
                        manager.clone(offset);
                        success = true;
                    }
                }
            }
        } else if (command == sRENAME) {
            if (commandsLength == COMM_TYPE_3) {
                if (isInt(commandsArray[1])) {
                    int offset = stoi(commandsArray[1]);
                    if (offset < manager.getQuantOfMatrixes()) {
                        manager.rename(offset, commandsArray[2]);
                        success = true;
                    }
                }
            }
        } else if (command == sDEL) {
            if (commandsLength == COMM_TYPE_2) {
                if (isInt(commandsArray[1])) {
                    int offset = stoi(commandsArray[1]);
                    if (offset < manager.getQuantOfMatrixes()) {
                        manager.del(offset);
                        success = true;
                    }
                }
            }
        }
        if (!success) {
            printWarning();
        }
    } while (!end);
}

void Interface::printWarning() {
    cout << WRNG_COMM << endl;
}

bool Interface::tabIsInt(string *array, int offset, int length) {
    bool areInts = true;
    for (int i = offset; i < (length + offset) && areInts; ++i) {
        if (!isInt(array[i])) areInts = false;
    }
    return areInts;
}

int *Interface::strToIntArray(string *commandsArray, int startOffset, int length) {
    auto result = new int[length];
    int offset = startOffset;
    for (int i = 0; i < length; ++i, offset++) {
        result[i] = stoi(commandsArray[offset]);
    }
    return result;
}

string* Interface::split(string str, char separator, int *length) {
    string *arrayOfWords;
    arrayOfWords = new string[1];
    string word;

    for (int i = 0; i <= str.length(); i++) {
        if ((i == str.length() || str[i] == separator) && word.length() > 0) {
            push(arrayOfWords, *length, word);
            (*length)++;
            word = "";
        } else if (str[i] != separator) {
            word += str[i];
        }
    }
    return arrayOfWords;
}

void Interface::push(string *&array, int length, string newWord) {
    string *newArray;
    newArray = new string[length + 1];

    for (int i = 0; i < length; i++) {
        newArray[i] = array[i];
    }

    newArray[length] = move(newWord);
    delete[] array;
    array = newArray;
}

bool Interface::isInt(string str) {
    if (str[0] != '-' && (str[0] < '0' || str[0] > '9')) return false;
    for (int i = 1; i < str.length(); i++) {
        if (str[i] < '0' || str[i] > '9') return false;
    }
    return true;
}

void Interface::printHelp() {
    std::cout << HELP_TO_PRINT << std::endl;
}
