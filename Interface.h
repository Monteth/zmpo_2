//
// Created by monteth on 10/24/17.
//

#ifndef ZMPO_2_INTERFACE_H
#define ZMPO_2_INTERFACE_H

#include <string>
class Interface{
public:

    void start();

private:

    void printHelp();

    std::string *split(std::string str, char separator, int *length);

    void push(std::string *&array, int length, std::string newWord);

    bool isInt(std::string str);

    int *strToIntArray(std::string *commandsArray, int startOffset, int length);

    bool tabIsInt(std::string *array, int offset, int length);

    void printWarning();
};

#endif //ZMPO_2_INTERFACE_H
