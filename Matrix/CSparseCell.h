//
// Created by monteth on 10/22/17.
//

#ifndef ZMPO_2_CSPARSECELL_H
#define ZMPO_2_CSPARSECELL_H

#include <iostream>
#include <cstring>



class CSparseCell{
public:
    CSparseCell(int quantOfDimensions, int* newCoords, int value);

    CSparseCell();

    explicit CSparseCell(CSparseCell *pCell);

    ~CSparseCell();

private:

    int *coords;
    int value;
    int quantOfDim;
    friend class CSparseMatrix;
};


#endif //ZMPO_2_CSPARSECELL_H
