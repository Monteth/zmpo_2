#include "CSparseMatrix.h"
#include "../Values.h"

CSparseMatrix::CSparseMatrix(int numberOfDimensions, const int *dimSizes, int defValue, std::string name) {
    this->defValue = defValue;
    this->name = name;
    this->quantOfDimensions = numberOfDimensions;
    this->dimSizes = new int[numberOfDimensions];
    this->quantOfCells = 0;
    this->cells = new CSparseCell *[quantOfCells];

    for (int i = 0; i < numberOfDimensions; ++i) {
        this->dimSizes[i] = dimSizes[i];
    }
    std::cout << sCONSTR_MATRIX << name << std::endl;
}

CSparseMatrix::CSparseMatrix() {
    this->defValue = 0;
    this->name = DEFAULT_NAME;
    this->quantOfDimensions = 0;
    this->dimSizes = new int[0];
    this->quantOfCells = 0;
    this->cells = new CSparseCell *[quantOfCells];

    for (int i = 0; i < quantOfDimensions; ++i) {
        this->dimSizes[i] = dimSizes[i];
    }
    std::cout << sCONSTR_MATRIX << name << std::endl;
}

CSparseMatrix::CSparseMatrix(CSparseMatrix *pMatrix) {
    this->cells = pMatrix->cells;
    this->defValue = defValue;
    this->quantOfCells = quantOfCells;
    this->quantOfDimensions = pMatrix->quantOfDimensions;
    this->dimSizes = new int[quantOfDimensions];
    memcpy(this->dimSizes, pMatrix->dimSizes, quantOfDimensions * INT_LENGTH);
    this->name = (pMatrix->name + sCOPIED_MATRIX);
    std::cout << sDESTR_MATRIX << name << std::endl;
}

std::string CSparseMatrix::getName() {
    return name;
}

void CSparseMatrix::setDimens(int quantOfDimensions, int *dimSizes) {
    if(quantOfCells == 0){
        this->quantOfDimensions;
        memcpy(this->dimSizes, dimSizes, quantOfDimensions * INT_LENGTH);
    }
}

void CSparseMatrix::setDef(int newDefValue) {
    defValue = newDefValue;
    for (int i = 0; i < quantOfCells; ++i) {
        if ((*cells[i]).value == defValue) {
            delValue((*cells[i]).coords);
        }
    }
}

void CSparseMatrix::setName(std::string newName) {
    this->name = newName;
}

std::string CSparseMatrix::valueString(int *pointer) {
    std::string result = "";
    result += "[";
    for (int j = 0; j < quantOfDimensions; ++j) {
        result += std::to_string(pointer[j]);
        if (j < quantOfDimensions - 1) {
            result += ", ";
        }
    }
    result += ("]: " + std::to_string(getValue(pointer)) + "\n");

    return result;
}

std::string CSparseMatrix::getString() {
    std::string result = "Matrix \"" + name + "\" size: [";

    for (int dimension = 0; dimension < quantOfDimensions; ++dimension) {
        result += std::to_string(dimSizes[dimension]);
        if (dimension != quantOfDimensions - 1) {
            result += ", ";
        }
    }
    result += "] values:";
    int *pointer = new int[quantOfDimensions];
    for (int i = 0; i < quantOfDimensions; ++i) {
        pointer[i] = 0;
    }
    bool end = false;
    bool found;

    result += valueString(pointer);

    while (!end) {
        found = false;
        for (int i = 0; i < quantOfDimensions && !found; ++i) {
            if (pointer[i] == dimSizes[i] - 1) {
                pointer[i] = 0;
            } else if (pointer[i] < dimSizes[i]) {

                pointer[i]++;
                found = true;
                i = 0;

                result += valueString(pointer);
            }
            if (i == quantOfDimensions - 1 && !found) {
                end = true;
            }
        }
    }
    return result;
}

void CSparseMatrix::printNotDefCells() {
    for (int i = 0; i < quantOfCells; ++i) {
        std::cout << (*cells[i]).value << ": ";;
        for (int j = 0; j < 3; ++j) {
            std::cout << (*cells[i]).coords[j];
        }
        std::cout << std::endl;
    }
}

/*void CSparseMatrix::shortDef(int cellOffset, int newValue) {
    defVal(cells[cellOffset]->coords, newValue);
}*/

void CSparseMatrix::defVal(int *coords, int newValue) {
    int currentVal = getValue(coords);
    if (currentVal == defValue) {
        if (newValue != defValue) {
            addSlot();
            cells[quantOfCells - 1] = new CSparseCell(quantOfDimensions, coords, newValue);
        }
    } else {
        if (newValue == defValue) {
            delValue(coords);
        } else if (currentVal != newValue) {
            setValue(coords, newValue);
        }
    }
}

int CSparseMatrix::getValue(const int *coords) {
    bool found = false;
    int result = defValue;
    bool fit;
    for (int i = 0; i < quantOfCells && !found; ++i) {
        fit = true;
        for (int j = 0; j < quantOfDimensions && fit; ++j) {
            if ((*cells[i]).coords[j] == coords[j]) {
                if (j == quantOfDimensions - 1) {
                    found = true;
                    result = (*cells[i]).value;
                }
            } else {
                fit = false;
            }
        }

    }
    return result;
}

CSparseMatrix::~CSparseMatrix() {
    delete[]cells;
    std::cout << sDESTR << name << std::endl;
}


void CSparseMatrix::delValue(int *coords) {
    int offset = 0;
    bool fit;
    for (int i = 0; i < quantOfCells; ++i) {
        fit = true;
        for (int j = 0; j < quantOfDimensions; ++j) {
            if ((*cells[i]).coords[j] != coords[j]) {
                fit = false;
            }
        }
        if (fit) {
            offset = i;
        }
    }

    CSparseCell **temp;
    temp = new CSparseCell *[quantOfCells - 1];
    int counter = 0;
    for (int k = 0; k < quantOfCells; ++k) {
        if (k != offset) {
            temp[counter] = cells[counter];
            counter++;
        }
    }
    quantOfCells--;
    delete[]cells;
    cells = temp;
}

void CSparseMatrix::setValue(int *coords, int newValue) {
    bool found = false;
    bool fit;
    for (int i = 0; i < quantOfCells && !found; ++i) {
        fit = true;
        for (int j = 0; j < quantOfDimensions && fit; ++j) {
            if ((*cells[i]).coords[j] == coords[j]) {
                (*cells[i]).value = newValue;
                found = true;
            } else {
                fit = false;
            }
        }

    }
}

void CSparseMatrix::addSlot() {
    CSparseCell **temp;
    temp = new CSparseCell *[quantOfCells + 1];
    for (int i = 0; i < quantOfCells; ++i) {
        temp[i] = cells[i];
    }
    quantOfCells++;
    delete[]cells;
    cells = temp;
}

int CSparseMatrix::getQuantOfDimensions() {
    return quantOfDimensions;
}

std::string CSparseMatrix::getShortString() {
    std::string result = "";
    result += "\"" + name + "\" size: [";
    for (int i = 0; i < quantOfDimensions; ++i) {
        result += std::to_string(dimSizes[i]);
        if (i < quantOfDimensions -1){
            result += ", ";
        }
    }

    result += "]";
    return result;
}

/*
CSparseMatrix &CSparseMatrix::operator=(const CSparseMatrix &source) {

}

*/
