//
// Created by monteth on 10/22/17.
//

#include "MatrixManager.h"
#include "../Values.h"



MatrixManager::MatrixManager() {
    this->matrixes = new CSparseMatrix*[quantOfMatrixes];
}

MatrixManager::MatrixManager(MatrixManager *manager) {
    this->quantOfMatrixes = manager->quantOfMatrixes;
    this->matrixes = new CSparseMatrix*[quantOfMatrixes];
    for (int i = 0; i < quantOfMatrixes; ++i) {
        this->matrixes[i] = manager->matrixes[i];
    }
}

MatrixManager::~MatrixManager() {
    delete[]matrixes;
}

void MatrixManager::addMatrix(int numberOfDimensions, int *dimSizes, int defValue) {
    addSlot();
    matrixes[quantOfMatrixes - 1] = new CSparseMatrix(numberOfDimensions, dimSizes, defValue, DEFAULT_NAME);
}

void MatrixManager::addMatrix(int numberOfDimensions, int *dimSizes, int defValue, std::string name) {
    addSlot();
    matrixes[quantOfMatrixes - 1] = new CSparseMatrix(numberOfDimensions, dimSizes, defValue, name);
}

void MatrixManager::addSlot() {
    quantOfMatrixes++;
    CSparseMatrix **temp;
    temp = new CSparseMatrix *[quantOfMatrixes];
    for (int i = 0; i < quantOfMatrixes - 1; ++i) {
        temp[i] = matrixes[i];
    }
    if (quantOfMatrixes!=0){
        delete[]matrixes;
    }
    matrixes = temp;
}

void MatrixManager::def(int matrixOffset, int *coords, int value) {
    matrixes[matrixOffset]->defVal(coords, value);

}

/*void MatrixManager::defShort(int matrixOffset, int cellOffset, int value) {
    matrixes[matrixOffset]->shortDef(cellOffset, value);
}*/

void MatrixManager::print(int matrixOffset) {
    std::cout << matrixes[matrixOffset]->getString() << std::endl;
}

void MatrixManager::printShort(int matrixOffset) {
    std::cout << "[" << matrixOffset << "] - " << matrixes[matrixOffset]->getShortString() << std::endl;
}

void MatrixManager::test() {
    matrixes[0]->printNotDefCells();
}

void MatrixManager::printList() {
    for (int i = 0; i < quantOfMatrixes; ++i) {
        printShort(i);
    }
}

void MatrixManager::del(int matrixOffset) {
    CSparseMatrix **temp;
    int counter = 0;
    temp = new CSparseMatrix *[quantOfMatrixes];
    for (int i = 0; i < quantOfMatrixes; ++i) {
        if (i != matrixOffset) {
            temp[counter] = matrixes[counter];
            counter++;
        }
    }
    quantOfMatrixes--;
    delete[]matrixes;
    matrixes = temp;
}

void MatrixManager::delall() {
    delete[]matrixes;
    matrixes = nullptr;
    quantOfMatrixes = 0;
}

void MatrixManager::clone(int matrixOffset) {
    addSlot();
    matrixes[quantOfMatrixes-1] = new CSparseMatrix(matrixes[matrixOffset]);
}

void MatrixManager::rename(int matrixOffset, std::string newName) {
    matrixes[matrixOffset]->setName(newName);
}

void MatrixManager::setDefault(int matrixOffset, int newDef) {
    matrixes[matrixOffset]->setDef(newDef);
}

void MatrixManager::setDim(int matrixOffset, int quantOfDim, int *dimSizes) {
    matrixes[matrixOffset]->setDimens(quantOfDim, dimSizes);
}

int MatrixManager::getQuantOfMatrixes() {
    return quantOfMatrixes;
}

int MatrixManager::getQuantOfDim(int offset) {
    return matrixes[offset]->getQuantOfDimensions();
}
