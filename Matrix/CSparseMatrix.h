//
// Created by monteth on 10/22/17.
//

#ifndef ZMPO_2_CSPARSEMATRIX_H
#define ZMPO_2_CSPARSEMATRIX_H

#include <iostream>
#include "CSparseCell.h"

class CSparseMatrix {
public:
    CSparseMatrix();

    CSparseMatrix(CSparseMatrix *pMatrix);

    CSparseMatrix(int numberOfDimensions, const int *dimSizes, int defValue, std::string name);

    ~CSparseMatrix();

/*    CSparseMatrix& operator = (const CSparseMatrix &source);*/

    void setValue(int *coords, int newValue);

    void delValue(int *coords);

    int getValue(const int *coords);

    /*void shortDef(int cellOffset, int newValue);*/

    void printNotDefCells();

    std::string getString();

    std::string valueString(int *pointer);

    void setName(std::string newName);

    std::string getShortString();

    void setDef(int newDefValue);

    void setDimens(int quantOfDimensions, int *dimSizes);

    std::string getName();

    void defVal(int *coords, int newValue);

    int getQuantOfDimensions();

private:
    std::string name;
    int quantOfDimensions;
    int *dimSizes;

    CSparseCell **cells;
    int quantOfCells;
    int defValue;

    void addSlot();
};

#endif //ZMPO_2_CSPARSEMATRIX_H

