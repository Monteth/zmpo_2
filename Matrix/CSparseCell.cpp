#include "CSparseCell.h"
#include "../Values.h"


CSparseCell::CSparseCell(){
        this->coords = new int();
    }

    CSparseCell::CSparseCell(CSparseCell *pCell) {
        this->quantOfDim = pCell->quantOfDim;
        this->value = pCell->value;
        this->coords = new int(quantOfDim);
        memcpy(this->coords, pCell->coords, static_cast<size_t>(quantOfDim * INT_LENGTH));
    }

    CSparseCell::CSparseCell(int quantOfDimensions, int* newCoords, int value){
        this->quantOfDim = quantOfDimensions;
        this->value = value;
        this->coords = new int[quantOfDimensions];
        memcpy(this->coords, newCoords, static_cast<size_t>(quantOfDim * INT_LENGTH));

        std::cout << sCONSTR_CELL << value << std::endl;
    }

CSparseCell::~CSparseCell() {
    delete[]coords;
    std::cout << sDESTR_CELL << value << std::endl;
}