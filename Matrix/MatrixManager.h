//
// Created by monteth on 10/22/17.
//

#ifndef ZMPO_2_MATRIXMANAGER_H
#define ZMPO_2_MATRIXMANAGER_H

#include "CSparseMatrix.h"



class MatrixManager {
public:
    MatrixManager();

    MatrixManager(MatrixManager *manager);

    void addMatrix(int numberOfDimensions, int* dimSizes, int defValue);
    void addMatrix(int numberOfDimensions, int* dimSizes, int defValue, std::string name);
    void def(int matrixOffset, int* coords, int value);
    void defShort(int matrixOffset, int cellOffset, int value);
    void print(int matrixOffset);
    void printList();
    void del(int matrixOffset);
    void delall();
    void clone(int matrixOffset);
    void rename(int matrixOffset, std::string newName);
    void setDefault(int matrixOffset, int newDef);
    void setDim(int matrixOffset, int quantOfDim, int *dimSizes);

    ~MatrixManager();

    void test();

    int getQuantOfMatrixes();

    int getQuantOfDim(int offset);

private:
    CSparseMatrix **matrixes;

    void addSlot();
    int quantOfMatrixes = 0;

    void printShort(int matrixOffset);
};


#endif //ZMPO_2_MATRIXMANAGER_H
